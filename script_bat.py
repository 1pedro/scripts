#!/usr/bin/env python
#-*- encoding: utf-8 -*-
# author: pedrojefferson.dev@gmail.com

import math, subprocess, re, sys

p = subprocess.check_output("upower -i $(upower -e | grep 'BAT') | grep -E 'state|to\ full|percentage'", shell = True);




def make_dict(p):
	d = {}
	for item in p.splitlines():
		item = item.decode('utf-8')
		nitem = item.split(':')
		l = list()
		for xitem in nitem:
			l.append(xitem.strip(' \t\n\r'))
		d[l[0]] = l[1]
	return d

info = make_dict(p)

total = int(info['percentage'].strip('%')) /10
triangles = int(total) * u'▸'.encode('utf-8')

import sys

color_green = '%{[32m%}'
color_yellow = '%{[1;33m%}'
color_red = '%{[31m%}'
color_reset = '%{[00m%}'
color_out = (
    color_green if total > 6
    else color_yellow if total > 4
    else color_red
)

if sys.version_info.major == 2:
	out = color_out + triangles + color_reset
else:
	out = color_out + triangles.decode('utf-8') + color_reset

#out = color_out + triangles.decode('utf-8') + color_reset
sys.stdout.write(str(out))
